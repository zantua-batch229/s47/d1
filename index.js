//console.log("hello")
// DISCUSSION #1

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullname = document.querySelector('#span-full-name');

// Using "keyup" event

/*
txtFirstName.addEventListener('keyup', (event) => {
	spanFullname.innerHTML = txtFirstName.value;

})

txtFirstName.addEventListener('keyup', (event) => {
	// the document where our JS is connected
	console.log(event.target);
	// the value that our "keyup" event listened
	console.log(event.target.value);
	
})
*/



// DISCUSSION #2

const updateFullName = () => {
	let firstName = txtFirstName.value	;
	let lastName = txtLastName.value;

	spanFullname.innerHTML = `${firstName} ${lastName}`;
}

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);